// smoothing.m defines Magma functions and intrinsics to check the Corti--Hacking--Petracci smoothing condition

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// amalgamate takes the set of subsets partition and returns partition with any subsets that meet X amalgamated
function amalgamate(partition, X)
    T := {S : S in partition | not IsEmpty(S meet X)};
    assert not IsEmpty(T);
    return Include(partition diff T, &join T);
end function;
    
// partition_of_edges_in_facet returns the partition of the edges of the polyhedral decomposition dec that lie in F.  Here P is a three-dimensional reflexive polytope, F is a facet of P, and dec is a polyhedral decomposition of the boundary of P.
function partition_of_edges_in_facet(P, F, dec)
    // sanity checks
    error if not Type(P) eq TorPol and Dimension(P) eq 3 and IsReflexive(P), "P should be a 3-dimensional reflexive polytope";
    error if not Dimension(Ambient(P)) eq 3, "P should lie in a 3-dimensional lattice";
    error if not F in Facets(P), "F should be a facet of P";
    error if not Type(dec) eq SeqEnum and Universe(dec) eq PowerStructure(TorPol), "dec must be a sequence of polytopes";
    // grab those polygons in dec that lie in F
    Ps := [ Q : Q in dec | Q subset F ];
    Es := SetToSequence( { e : e in Edges(Q), Q in Ps });
    // partition is the partition of the edges
    partition := { {e} : e in Es };
    // update the partition depending on the triangulation
    for Q in Ps do
    	if NumberOfVertices(Q) eq 3 then
    	    partition := amalgamate(partition, {e : e in Edges(Q)});
    	elif NumberOfVertices(Q) eq 4 then
    	    e := Edges(Q)[1];
    		// find the opposite edge
    		opp_edges := [f : f in Edges(Q) | IsEmpty(e meet f)];
    	    assert #opp_edges eq 1;
    	    e_opp := Representative(opp_edges);
    	    partition := amalgamate(partition, {e,e_opp});
    	    partition := amalgamate(partition, {f : f in Edges(Q) | f ne e and f ne e_opp});
    	else
    	    error "Something is wrong: the polyhedral decomposition should have only triangles and squares";
    	end if;
    end for;
    return partition;
end function;

// partition_of_edge returns the partition defined by F of the edges of the polyhedral decomposition dec that lie in e.  Here P is a three-dimensional reflexive polytope, F is a facet of P, and e is an edge of f.  dec is a polyhedral decomposition of the boundary of P.
function partition_of_edge(P, F, e, dec)
    partition := partition_of_edges_in_facet(P, F, dec);
    result := {Universe(partition)| };
    for S in partition do
    	T := {f : f in S | f subset e};
    	if not IsEmpty(T) then
    	    Include(~result, T);
    	end if;
    end for;
    return result;
end function;

// two_partitions returns the partitions of the edge e of P defined by the polyhedral decomposition of the facets containing e.  Here P is a three-dimensional reflexive polytope, e is an edge of P, and dec is a polyhedral decomposition of the boundary of P.
function two_partitions(P, e, dec)
    Fs := [F : F in Facets(P) | e subset F];
    assert #Fs eq 2;
    return partition_of_edge(P, Fs[1], e, dec), partition_of_edge(P, Fs[2], e, dec);
end function;

// same_component returns true if and only if e1 and e2 lie in the same component of partition
function same_component(partition, e1, e2)
    candidates_1 := [Universe(partition) | S : S in partition | e1 in S];
    candidates_2 := [Universe(partition) | S : S in partition | e2 in S];
    assert #candidates_1 eq 1;
    assert #candidates_2 eq 1;
    return candidates_1 eq candidates_2;
end function;

// class_of_fiber returns a set of edges in dec that lie over the endpoints of E and are together homologous to the sum of the fibers of the P1-bundles lying over E that run from the level specified by e1 to the level specified by e2.  Here dec is a polyhedral decomposition of the boundary of the three-dimensional reflexive polytope P, e is an edge of P, e1 and e2 are edges of dec, and e1 and e2 lie over e.
function class_of_fiber(e1, e2, e, P, dec)
    // sanity checks
    error if not Type(P) eq TorPol and Dimension(P) eq 3 and Dimension(Ambient(P)) eq 3 and IsReflexive(P), "P must be a three-dimensional reflexive polytope in a three-dimensional lattice";
    error if not e in Edges(P), "E must be an edge of P";
    error if not Dimension(e1) eq 1 and Dimension(e2) eq 1 and e1 subset e and e2 subset e, "e1 and e2 must be edges lying over e";
    // choose a facet incident to e
    F := Representative([F : F in Facets(P) | e subset F]);
    // grab the vertices on e that give the P1-bundles
    vs := InteriorPoints(Polytope(Vertices(e1) cat Vertices(e2)));
    // prepare the result
    Es := [PowerStructure(TorPol)|];
    for v in vs do
		// find the linear functional which vanishes on e and is positive on F
		u := SupportingHyperplane(Cone([v : v in Vertices(F)]), Cone([v : v in Vertices(e)]));
		// add the appropriate edges to Es
		for f in {PowerStructure(TorPol)| f : f in Edges(Q), Q in dec | v in Vertices(f) and f subset F and not f subset e} do
			// grab the other vertex of f
			w := Representative(SequenceToSet(Vertices(f)) diff {v});
			// add the edge with the appropriate multiplicity
			multiplicity := Abs(u*w);
			Es cat:= [PowerStructure(TorPol)| f : i in [1..multiplicity]];
		end for;
    end for;	
    return Es;	
end function;	


// precise_smooths returns true, N, Es if the polyhedral decomposition dec satisfies the Corti--Hacking--Petracci sufficient condition to define a smoothing Y_t of the toric variety associated to the spanning fan of P, and false, _, _ otherwise.  Here N is the number of (-1,-1) curves in Y_t associated to the polyhedral decomposition dec, Es is a set of sequences of edges of dec representing the (-1,-1) curves in Y_t, P is a three-dimensional reflexive polytope, and dec is a polyhedral decomposition of the boundary of P.
function precise_smooths(P, dec)
    multiplicities := [Integers()|];
    Es := [PowerSequence(PowerStructure(TorPol))|];
    for e in Edges(P) do
		part_1, part_2 := two_partitions(P, e, dec);
		// degree is the degree of the deformation sheaf on the curves given by edges of dec that lie over e
		degree := NumberOfPoints(DualFace(P,e))-1;
		// upstairs_edges is the edges of dec that lie over e
		upstairs_edges := &join(part_1);
		// we get (-1, -1) curves for each pair of distinct edges that lie over e
		for x in { {e1, e2} : e1, e2 in upstairs_edges | e1 ne e2 } do
		    e1, e2 := Explode(SetToSequence(x));
		    // set correction to the number (0, 1, or 2) of partitions where e1, e2 are in the same component
		    correction := Multiplicity([same_component(part_1, e1, e2), same_component(part_2, e1, e2)],true);
		    if correction gt degree then
				// the smoothing doesn't exist
				return false, _, _, _;
		    end if;
		    if correction lt degree then
				// we need to add the corresponding (-1,-1) curves to Es
				Append(~Es, class_of_fiber(e1, e2, e, P, dec));
				Append(~multiplicities,degree - correction);
		    end if;
		end for;
    end for;
    return true, &+multiplicities, Es, multiplicities;
end function;

//////////////////////////////////////////////////////////////////////
// Intrinsics
//////////////////////////////////////////////////////////////////////


intrinsic CHPSmoothnessCheck(P::TorPol, dec::SeqEnum[TorPol]) -> BoolElt, RngIntElt, SeqEnum[SeqEnum[TorPol]], SeqEnum[RngIntElt]
    {True if and only if the polyhedral decomposition dec of the 3-dimensional reflexive polytope P satisfies the Corti--Hacking--Petracci sufficient condition to define a smoothing.  If true, also returns the number of (-1,-1) curves in the general fiber of such a smoothing, and a sequence of sequences of edges of dec representing the span of these classes in homology, and the multiplicities of these curves.}
    require IsReflexive(P) and Dimension(P) eq 3 and Dimension(Ambient(P)) eq 3: "P must be a three-dimensional reflexive polytope in a three-dimensional lattice";
    ok, N, Es, multiplicities := precise_smooths(P, dec);
    if ok then
    	return ok, N, Es, multiplicities;
    else
    	return false, _, _, _;
    end if;
end intrinsic;