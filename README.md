# Fano_topology_dim_3

Repository containing Magma code for computing the Betti numbers of smoothings of three-dimensional toric Fano varieties.

Written in 2019 by 

Tom Coates <t.coates@imperial.ac.uk>  
Alessio Corti <a.corti@imperial.ac.uk>  
Genival Fernandes Da Silva Jr <g.junior@imperial.ac.uk>

To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public
domain worldwide. This software is distributed without any warranty. You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

If you make use of this code in an academic or commercial context, please acknowledge this by including a reference or citation to the paper
"On the Topology of Fano Smoothings"; by Tom Coates, Alessio Corti, and Genival Da Silva Jr, 2019.


============================================================

The accompanying Magma scripts allow the computation of the Betti numbers of smoothings of three-dimensional toric Fano varieties: see our paper "On the Topology of Fano Smoothings" for details. The scripts require Magma version 2.22-9, tag v1.0.2 of the Fanosearch codebase, and version 0.17.8 of TOPCOM. Magma can obtained from the Computational Algebra Group at the University of Sydney, Australia; see

http://magma.maths.usyd.edu.au/magma/

The Fanosearch codebase can be obtained from 

https://bitbucket.org/fanosearch/magma-core

TOPCOM can be obtained from 

http://www.rambau.wm.uni-bayreuth.de/Software/TOPCOM-0.17.8.tar.gz

To use the scripts, ensure that your working directory contains the scripts, start Magma, and enter:

`AttachSpec("smoothing.spec");`

This provides the following Magma intrinsics:

```AnDecompositions
CHPSmoothnessCheck
DualFace
FineMixedSubdivisions
PolyhedralDecompositionProcess
PolyhedralDecompositions
ToString
```

A description of each intrinsic can be found by entering the intrinsic name followed by a semicolon. For example, entering:

`AnDecompositions;`

produces output as follows:


    Intrinsic 'AnDecompositions'

    Signatures:

        (P::TorPol) -> SeqEnum

            A sequence containing all Minkowski decompositions of P into A_n 
            triangles and line segments. P must be a polygon in a two-dimensional 
            lattice.


