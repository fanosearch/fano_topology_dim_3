// An.m defines Magma functions and intrinsics to compute Minkowski decompositions of polygons into An triangles.

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// is_An_triangle returns true if and only if P is a triangle of type A_n.  Assumes that P is a polygon in a 2-dimensional lattice.
function is_An_triangle(P)
	// P must be an empty triangle
	if NumberOfVertices(P) ne 3 or NumberOfInteriorPoints(P) ne 0 then
		return false;
	end if;
	// P must have edge lengths [1,1,n]
	edge_lengths := [NumberOfPoints(E) - 1 : E in Edges(P)];
	s := Sort(edge_lengths);
	if s[1..2] ne [1,1] then
		return false;
	end if;
	return true;
 end function;

//////////////////////////////////////////////////////////////////////
// Intrinsics
//////////////////////////////////////////////////////////////////////

intrinsic AnDecompositions(P::TorPol) -> SeqEnum[SeqEnum[TorPol]]
{A sequence containing all Minkowski decompositions of P into A_n triangles and line segments.  P must be a polygon in a two-dimensional lattice.}
    require Dimension(P) eq 2 and Dimension(Ambient(P)) eq 2: "P must be a two-dimensional polytope in a two-dimensional lattice";
    decs := MinkowskiDecomposition(P);
    admissible_decs := [];
    for dec in decs do
		this_dec := [Universe(dec)|];
		this_translation := Zero(Ambient(P));
		success := true;
		for P in dec do
		    if Dimension(P) eq 0 then
				this_translation := Representative(Vertices(P));
				continue;
		    end if;
		    if is_An_triangle(P) or Dimension(P) eq 1 then
				Append(~this_dec, P);
		    else
				success := false;
				break;
		    end if;
		end for;
		if success then
		    // translate the first factor in the decomposition
		    this_dec[1] +:= this_translation;
		    Append(~admissible_decs, this_dec);
		end if;
    end for;
    return admissible_decs;
end intrinsic;
