// utilities.m defines utility functions

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// A string representation of the set or sequence of polytopes Ps.
// This is the sequence of the sequence of vertices, where vertices
// are represented by integer sequences.  If Ps is a set, then the
// sequence is sorted before returning.
function polytopes_to_string(Ps)
    error if not (Type(Ps) eq SeqEnum or Type(Ps) eq SetEnum) and Universe(Ps) eq PowerStructure(TorPol), "Ps must be a set or sequence of polytopes";
    vertex_seq := Sort([ [Eltseq(v) : v in Sort(Vertices(P))] : P in Ps]);
    return ToString(vertex_seq);
end function;

//////////////////////////////////////////////////////////////////////
// Intrinsics
//////////////////////////////////////////////////////////////////////

intrinsic ToString(Ps::SeqEnum[TorPol]) -> MonStgElt
{A string representation of the set of polytopes Ps. This is the sequence of the sequence of vertices, where vertices are represented by integer sequences.}
	return polytopes_to_string(Ps);
end intrinsic;

intrinsic ToString(Ps::SetEnum[TorPol]) -> MonStgElt
{A string representation of the set of polytopes Ps. This is the sequence of the sequence of vertices, where vertices are represented by integer sequences. The sequence returned is sorted.}
	return polytopes_to_string(Ps);
end intrinsic;

intrinsic DualFace(P::TorPol,F::TorPol) -> TorPol
{ The face of Dual(P) that is dual to F. }
    return Polytope(Rays(DualFaceInDualFan(P,F)));
end intrinsic;
