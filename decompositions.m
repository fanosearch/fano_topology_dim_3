// decompositions.m defines Magma functions and intrinsics to compute polyhedral decompositions of the boundary of a polytope

// The cache for Minkowski and polyhedral decompositions.  Do not access directly!
dec_cache:=NewStore();

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// initialise_cache initialises the cache
procedure initialise_cache()
    error if StoreIsDefined(dec_cache,"init"), "initialise_cache called twice";
    StoreSet(dec_cache, "all_Ts", AssociativeArray(Integers()));
    StoreSet(dec_cache, "all_Ps", AssociativeArray(Integers()));
    StoreSet(dec_cache, "init", true);
end procedure;

// Returns parallel sequences Ts, Ps of Minkowski decompositions and polyhedral
// decompositions of the 3d canonical facet with given id.  If this data is
// already cached, it retrieves it from the cache; otherwise it computes Ts, Ps
// := f(id), stores these values in the cache with key id, and returns them.
function decompositions_from_cache(id, f)	
    // initialise the cache, if necessary
    if not StoreIsDefined(dec_cache,"init") then
        initialise_cache();
    end if;
    // retrieve the cached data, if it exists
    all_Ts := StoreGet(dec_cache, "all_Ts");
    all_Ps := StoreGet(dec_cache, "all_Ps");
    if id in Keys(all_Ts) then
        return all_Ts[id], all_Ps[id];
    end if;
    // compute and store the data
    this_Ts, this_Ps := f(id);
    all_Ts[id] := this_Ts;
    all_Ps[id] := this_Ps;
    StoreSet(dec_cache, "all_Ts", all_Ts);
    StoreSet(dec_cache, "all_Ps", all_Ps);
    return this_Ts, this_Ps;
end function;

// Returns the function of one integer variable, id, that reads from the given
// table in the given key-value database and returns parallel sequences Ts, Ps
// of Minkowski decompositions and polyhedral decompositions of the 3d canonical
// facet with given id.
function read_from_database(db, table)
    return function(id)
        // build the conversion functions
        cfs := AssociativeArray();
        cfs["facet_id"] := StringToInteger;
        cfs["decomposition_id"] := StringToInteger;
        cfs["Minkowski_decomposition"] := func<xx|eval(xx)>;
        cfs["polyhedral_decomposition"] := func<xx|eval(xx)>;
        keys := Keys(cfs);
        // build the selector
        selector := AssociativeArray();
        selector["facet_id"] := id;
        // read from the database
        Ts := [PowerSequence(PowerStructure(TorPol))|];
        Ps := [PowerSequence(PowerStructure(TorPol))|];
        P := KeyValueDbProcess(db, table, keys, selector : skip_unknown:=false, require_all_keys:=true, conversion_funcs:=cfs, limit:=false);
        for x in P do
            Append(~Ts, [PowerStructure(TorPol)| Polytope(S) : S in x["Minkowski_decomposition"]]);
            Append(~Ps, [PowerStructure(TorPol)| Polytope(S) : S in x["polyhedral_decomposition"]]);
        end for;
        return Ts, Ps;
    end function;
end function;

// Returns the function of one integer variable, id, that uses TOPCOM to compute
// parallel sequences Ts, Ps of Minkowski decompositions and polyhedral
// decompositions of the 3d canonical facet with given id.  The argument is the
// path to TOPCOM.
function compute_with_TOPCOM(path)
    return function(id)
        Ts := [PowerSequence(PowerStructure(TorPol))|];
        Ps := [PowerSequence(PowerStructure(TorPol))|];
        for dec in AnDecompositions(PolytopeCanonicalFanoDim3Facet(id)) do
            for poly_dec in FineMixedSubdivisions(dec : TOPCOM_path := path) do
                Append(~Ts, dec);
                Append(~Ps, SetToSequence(poly_dec));
            end for;
        end for;
        return Ts, Ps;
    end function;
end function;

// Returns 	id, phi, v where id is the ID in the 3D canonical facets database of F, and x -> phi(x) + v is an affine-linear map that takes PolytopeCanonicalFanoDim3Facet(id) to F
function id_and_map(F)
	id := PolytopeCanonicalFanoDim3FacetID(F);
	F_sub, phi_sub, v_sub := PolyhedronInSublattice(F);
	normal_form := PolytopeCanonicalFanoDim3Facet(id);
	for this_v in Vertices(F_sub) do
		ok, phi_aff, v_aff := IsEquivalent(normal_form, F_sub - this_v);   
		if ok then
			phi := phi_aff * phi_sub;   
			v := phi_sub(v_aff + this_v) + v_sub;
			assert Image(phi, normal_form) + v eq F;
			return id, phi, v;
		end if;
	end for;
	error "Something is wrong: can't find affine equivalence when theory says there should be one.";
end function;
	
// Returns v such that P+v eq Q.  Raises an error if no such v exists
function find_translation(P,Q)
	v := Vertices(P)[1];
	for w in Vertices(Q) do
		if P + w - v eq Q then
			return w-v;
		end if;
	end for;
	error "Can't find translation matching P and Q";
end function;

// decompositions returns bool, all_Ts, all_Ps where bool is true if and only if
// every facet of P has an admissible Minkowski decomposition, and in that case
// all_Ts, all_Ps are parallel sequences of Minkowksi and polyhedral
// decompositions of each facet of P.  These decompositions are obtained and
// cached using lookup_func.  If bool is false then the other return values are
// _.
function decompositions_for_facets(P, lookup_func)
    Fs := Facets(P);
    // prepare the return values
    all_Ts := [PowerSequence(PowerSequence(PowerStructure(TorPol)))|];
    all_Ps := [PowerSequence(PowerSequence(PowerStructure(TorPol)))|];
    // walk the facets
    for F in Fs do
		id, phi, v := id_and_map(F);
        Ts_for_F, Ps_for_F := decompositions_from_cache(id, lookup_func);
		this_Ts := [ PowerSequence(PowerStructure(TorPol))| [ Image(phi, P) : P in Ts ] : Ts in Ts_for_F];
		this_Ps := [ PowerSequence(PowerStructure(TorPol))| [ Image(phi, P) + v : P in Ps ] : Ps in Ps_for_F];
		if IsEmpty(this_Ts) then
		    // there are no admissible Minkowski decompositions of this facet, hence none of P
			return false, _, _;
		end if;
		// translate the first element of each member of this_Ts, so that it is a Minkowski decomposition of the correct thing
		for i in [1..#this_Ts] do
			this_Ts[i][1] +:= find_translation(&+this_Ts[i], F);
		end for;
		// sanity checks
		assert &and[&+Ts eq F : Ts in this_Ts];
		assert &and[Polytope({v : v in Vertices(P), P in Ps}) eq F : Ps in this_Ps];
		// record the decompositions
		Append(~all_Ts, this_Ts);
    	Append(~all_Ps, this_Ps);
    end for;
    return true, all_Ts, all_Ps;
end function;

// returns the current polyhedral decomposition.  The argument is the user process data for PolyhedralDecompositionProcess.
function get_current(data)
    pt := Eltseq(Current(data["points"]));
    all_Ts := data["all_Ts"];
    all_Ps := data["all_Ps"];
	this_T := [all_Ts[i][pt[i]] : i in [1..#all_Ts]];
	this_P := &cat[all_Ps[i][pt[i]] : i in [1..#all_Ps]];
    return <this_P,this_T>;
end function;
    
// returns the init function for PolyhedralDecompositionProcess, storing the given data as user process data.
function make_init(data)
    // Handle the empty case
    if data["is_empty"] then
        return function(P)
            return false, _;
        end function;
    end if;
    // Return the init function
    return function(P)
        this_data := data;
        result := get_current(this_data);
        Advance(~this_data["points"]);
        SetUserProcessData(P, this_data);
        return true, result;
    end function;
end function;

// This is the next_value function for PolyhedralDecompositionProcess.  It returns true, v if the next value is v, and false, _ if the process is complete.
function next(P)
    // grab the process data
    data := GetUserProcessData(P);
    // have we finished?
    if IsEmpty(data["points"]) then
        return false, _;
    end if;
    // advance the process and return
    result := get_current(data);
    Advance(~data["points"]);
    SetUserProcessData(P, data);
    return true, result;
end function;

//////////////////////////////////////////////////////////////////////
// Intrinsics
//////////////////////////////////////////////////////////////////////

intrinsic PolyhedralDecompositionProcess(P::TorPol : db:=false, table:=false, TOPCOM_path:=false) -> Process
    {A process returning tuples <P,T> where

* T is a sequence [S1,S2,...] where Si is a Minkowski decomposition of the i-th element of Facets(P) into An triangles and line segments;

* P is a polyhedral decomposition of the boundary of P, given by calling FineMixedSubdivisions on each of [S1,S2,...] and choosing one of the resulting fine mixed subdivisions for each facet.

The parameters db, table, TOPCOM_path specify whether to look up the facet decompositions in a key-value database, specifically in the given table in the key-value database db, or to compute them using TOPCOM.  In the latter case, TOPCOM_path must be set to the path to the TOPCOM executable points2finetriangs.
}
    require Dimension(P) eq 3 and Dimension(Ambient(P)) eq 3: "P must be a 3-dimensional polytope in a 3-dimensional lattice";
    if TOPCOM_path cmpeq false then
        require (not db cmpeq false) and (not table cmpeq false): "Either TOPCOM_path must be specified or both db and table must be specified";
    end if;
    // grab the facets
    Fs := Facets(P);
    // prepare to compute or look up the decompositions of the facets
    if db cmpeq false or table cmpeq false then
        // we need to compute the decompositions
        lookup_func := compute_with_TOPCOM(TOPCOM_path);
    else
        lookup_func := read_from_database(db, table);
    end if;
    // build parallel sequences of Minkowski and polyhedral decompositions of each facet of P
    ok, all_Ts, all_Ps := decompositions_for_facets(P, lookup_func);
    // wrap this up for the process
    data := AssociativeArray();
    data["is_empty"] := not ok;
    if ok then
        data["all_Ts"] := all_Ts;
        data["all_Ps"] := all_Ps;
        // make a process to walk over all points in the relevant box
        data["points"] := PointProcess(BoundingBox(Polytope([[1 : T in all_Ts], [#T : T in all_Ts]])));
    end if;
    return UserProcess("Polyhedral decompositions", make_init(data), next);
end intrinsic;
    

intrinsic PolyhedralDecompositions(P::TorPol : db:=false, table:=false, TOPCOM_path:=false) -> SeqEnum[SeqEnum[TorPol]], SeqEnum[SeqEnum[TorPol]]
    {Parallel sequences Ps, Ts where:

* an element of Ts is a sequence [S1,S2,...] where Si is a Minkowski decomposition of the i-th element of Facets(P) into An triangles and line segments;

* an element of Ps is a polyhedral decomposition of the boundary of P, given by calling FineMixedSubdivisions on each of [S1,S2,...] and choosing one of the resulting fine mixed subdivisions for each facet.

The parameters db, table, TOPCOM_path specify whether to look up the facet decompositions in a key-value database, specifically in the given table in the key-value database db, or to compute them using TOPCOM.  In the latter case, TOPCOM_path must be set to the path to the TOPCOM executable points2finetriangs.
}
    require Dimension(P) eq 3 and Dimension(Ambient(P)) eq 3: "P must be a 3-dimensional polytope in a 3-dimensional lattice";
    if TOPCOM_path cmpeq false then
        require (not db cmpeq false) and (not table cmpeq false): "Either TOPCOM_path must be specified or both db and table must be specified";
    end if;
    // prepare the return values
    Ts := [PowerSequence(PowerSequence(PowerStructure(TorPol)))|];
    Ps := [PowerSequence(PowerStructure(TorPol))|];
    // do the computation
    for x in PolyhedralDecompositionProcess(P : db:=db, table:=table, TOPCOM_path:=TOPCOM_path) do
        Append(~Ps, x[1]);
        Append(~Ts, x[2]);
    end for;
    return Ps, Ts;
end intrinsic;
