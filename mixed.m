// mixed.m defines Magma functions and intrinsics to compute fine mixed subdivisions of a polygon

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// validate returns true, _ if and only if Ps is a non-empty sequence of polytopes of dimension at most 2, lying in a common 2-dimensional ambient lattice.  Otherwise it returns false, S where S is a string describing the condition that failed.
function validate(Ps)
	if Type(Ps) ne SeqEnum or #Ps eq 0 then 
		return false, "Ps must be a non-empty sequence of polytopes of dimension at most two";
	end if;
	if not Universe(Ps) cmpeq PowerStructure(TorPol) then
		return false,  "Ps must be a sequence of polytopes of dimension at most two";
	end if;
	L := Ambient(Ps[1]);
	if not Dimension(L) eq 2 then
		return false, "The polytopes in Ps must have two-dimensional ambient lattice";
	end if;
	for P in Ps do
		if Ambient(P) ne L then
			return false, "The polytopes in Ps must all lie in the same lattice";
		end if;
	end for;
	return true, _;
end function;

// make_input_matrix makes a point configuration, in the format expected by TOPCOM, from the lattice points in the Cayley sum of the Ps.  Assumes that Ps has been validated using validate.
function make_input_matrix(Ps)
	n := #Ps;
	// build the matrix
	cols := [PowerSequence(Integers())|];
	for i in [1..n] do
		tail := [0 : j in [1..n]];
		tail[i] := 1;
		cols cat:= [ Eltseq(p) cat tail : p in Points(Ps[i]) ];
	end for;
	return Matrix(cols);
end function;

// parse_TOPCOM runs TOPCOM to compute the fine triangulations of the Cayley sum of the Ps.  The optional argument TOPCOM_path specifies the path to the TOPCOM command points2finetriangs.  The triangulations are returned as sequences of sequences of integer sequences, the latter representing the vertices of a simplex.  Assumes that Ps has been validated using validate_Ps, and that limit is either false or a positive integer.
function parse_TOPCOM(Ps : TOPCOM_path:=false, limit := false)
	if TOPCOM_path cmpeq false then
		TOPCOM_path := "points2finetriangs";
	end if;
    if limit cmpeq false then
        limit_cmd := "";
    else
        // there are 2 header lines, so we add 2 to limit
        limit_cmd := Sprintf("| head -n %o", limit+2);
    end if;
	// build the command string
	point_matrix := make_input_matrix(Ps);
	input := ToString(RowSequence(point_matrix));
	P := Polytope(RowSequence(point_matrix));
	G := AutomorphismGroup(P);
	rows := Rows(point_matrix);
	symmetries := ToString([[Index(rows, Eltseq(x*g)) - 1 : x in rows] : g in Generators(G)]);
	cmd := Sprintf("echo \"%o %o\" | %o 2>&1 %o", input, symmetries, TOPCOM_path, limit_cmd);
	// run the command
	ok, s := PipeCommand(cmd);
	assert ok;
	// parse the output
	lines := Split(s);
	error if not lines[1] eq "Evaluating Commandline Options ...", "Error parsing output line 1";
	error if not lines[2] eq "... done.", "Error parsing output line 2";
	results := [];
	for line in lines[3..#lines] do
		// grab the last part of the string
		x := Split(line,":")[3];
		// remove the trailing "];"
		S := eval(x[1..#x-2]);
		// write out the vertices of the simplices in the triangulation
		this_tri := [ [rows[i+1] : i in T] : T in S ];
		// add the orbit of this triangulation under the automorphism group
		these_tri := [[[ Eltseq(x*g) : x in T ] : T in this_tri ] : g in G];
		results cat:= these_tri;
	end for;
    if limit cmpeq false or #results le limit then
        return results;
    else 
        return results[1..limit];
    end if;
end function;

// basis_vector returns a sequence of length n with zeroes in all except the i-th place and a 1 in the i-th place
function basis_vector(i,n)
	result := [Zero(Integers()) : j in [1..n]];
	result[i] := 1;
	return result;
end function;

// has_length_one_edges returns true if and only if the polytope P has all edges of length 1
function has_length_one_edges(P)
	verts:=Vertices(P);
	for I in EdgeIndices(P) do
		idx1,idx2:=Explode(SetToSequence(I));
		if not IsPrimitive(verts[idx1] - verts[idx2]) then
			return false;
		end if;
	end for;
	return true;
end function;

// is_valid_subdivision returns true if and if all polytopes in the subdivision have edges of length one
function is_valid_subdivision(Ps)
	return &and[has_length_one_edges(P) : P in Ps];
end function;

//////////////////////////////////////////////////////////////////////
// Intrinsics
//////////////////////////////////////////////////////////////////////

// FineMixedSubdivisions returns all fine mixed subdivisions of the Minkowksi sum of the Ps determined by Ps.  We require all polygons in such a subdivision to have all edges of length 1.
intrinsic FineMixedSubdivisions(Ps::SeqEnum : TOPCOM_path := false, limit:=false) -> SeqEnum
{ The set of all fine mixed subdivisions of P, where P is the Minkowski sum of the polygons Ps, determined by Ps.  Each polygon in the fine mixed subdivision is required to have edges of length 1.  If the optional parameter limit is set to a positive integer then at most that many fine mixed subdivisions are returned.}
	ok, msg := validate(Ps);
	require ok: msg;
    if not limit cmpeq false then
        require limit in Integers() and limit gt 0: "the optional parameter 'limit' must be a positive integer";
    end if;
	// grab the triangulations of the Cayley sum
	tri_vertices := parse_TOPCOM(Ps : TOPCOM_path:=TOPCOM_path, limit:=limit);
	Ts := [ [ Polytope(vs) : vs in tri] : tri in tri_vertices ];
	// intersect with the relevant affine subspace
	n := #Ps;
	V := &meet[HyperplaneToPolyhedron(basis_vector(i+2,n+2), 1/n) : i in [1..n]];
	Ss := [ [ s meet V : s in T] : T in Ts];
	// project to the first two co-ordinates and rescale
	L := Ambient(Ps[1]);
	phi := hom<Ambient(V)->L|[L.1,L.2] cat [Zero(L) : i in [1..n]]>;
	rescaled_Ss := { { n*Image(phi,Q) : Q in S } : S in Ss };
	// return only those fine mixed subdivisions where all polygons have edges of length 1
	return { S : S in rescaled_Ss | is_valid_subdivision(S) };
end intrinsic;
